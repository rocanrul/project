#pragma once
// La clase AdminEntidad se encarga de gestionar a TODAS las entidades (Y SUB ENTIDADES) y les indica: QUE deben hacer, COMO deben hacerlo y CUANDO deben hacerlo
#include "Includes.h"
#include "Jugador_1.h"
#include "Mapa.h"
#include "Objeto.h"
#include "Camara.h"
#include "Puntero.h"
#include "Control.h"
#include "Pnj.h"
#include "Actor_data.h"

class AdminEntidad
{
public:
	Jugador_1 jugador1;
	Mapa mapa;
	Objeto objeto;
	Camara camara;
	Puntero puntero;
	Control control;
	Pnj pnj;
	Actor_data actor_data;
	std::vector <Objeto> vector_obj;
	std::vector <Actor_data> v_actor_data;
	//vector <Objeto> muro_h;
	bool espacio = false;
	AdminEntidad();
	~AdminEntidad();
	void cargar_entidades();
	void guardar_entidades(bool n);
	void desplazar_entidades();
	void desplazar_camara(int rx, int ry);
	void detectar_colisiones();
	float rx, ry;
	void calcular_dimension(int rx_actual, int rx_ant, int ry_actual, int ry_ant, int tam_tile);
	//float calcular_posicion_x(int x);
	//float calcular_posicion_y(int y);
	int tile_size = 60;
	int incremento;
	std::vector <SDL_Rect> v_sobrePos;
	void detectar_sobrepos(int  id, int y , std::vector <int> col_obj, std::vector <int> col_obj2);
	void ejecutar_acciones();
	//std::vector<std::array<int, 9>> v_ent;
	//bool colision;
	//bool colision_abajo;
	//bool colision_arriba;
	//bool colision_derecha;
	//bool colision_izquierda;
	//vector <int> num_obj;
};

