#pragma once
#include "includes.h"
#include "Jugador_1.h"
class Camara
{
public:
	Jugador_1 juga;
	Camara();
	SDL_Rect cam{ 0, 0, 800, 600 };
	void desplazar_cam(int a, int d, int s, int w, float rx, float ry, int incremento);
	void seguir_jugador(int x, int y, bool derecha, bool izquierda, bool abajo, bool arriba, bool col_der, bool col_izq, bool col_arr, bool col_aba, float rx, float ry, int incremento);
	~Camara();
};

