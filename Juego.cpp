#include "Juego.h"
#include "Pantalla.h"
#include "AdminEntidad.h"


Pantalla pantalla;
AdminEntidad admin_ent;

Juego::Juego()
{
	pantalla.ventana = NULL;
	corriendo = true;
}


Juego::~Juego()
{
}


int Juego::ejecutando()
{
	if (!pantalla.iniciar_pantalla())
	{
		return -1;
	}
		
		std::cout << "la pantalla inicio correctamente" << endl;


	if(!pantalla.cargar_contenido())
	{
		std::cout << "error al cargar el contenido" << endl;
	}

	admin_ent.calcular_dimension(pantalla.screenWidth, pantalla.ancho_pantalla, pantalla.screenHeight, pantalla.alto_pantalla, pantalla.tam_tile_x);
	admin_ent.cargar_entidades();
	pantalla.mapa_alto = admin_ent.mapa.alto;
	pantalla.mapa_ancho = admin_ent.mapa.ancho;
	

	std::cout << "el contenido cargo correctamente" << endl;

	while (corriendo)
	{

		if (pantalla.pantalla_completa(admin_ent.control.alt_l, admin_ent.control.enter))
		{
			admin_ent.calcular_dimension(pantalla.screenWidth, pantalla.ancho_pantalla, pantalla.screenHeight, pantalla.alto_pantalla, pantalla.tam_tile_x);
			//admin_ent.cargar_entidades();
		}
		
		frameStart = SDL_GetTicks();
		pantalla.limpiar_pantalla();
		
		//admin_ent.control.entradas();
		if ((admin_ent.control.salir(&admin_ent.control.evento) == true) || (admin_ent.control.cerrar == true))
		{
			std::cout << "el juego se cerro correctamente" << endl;
			corriendo = false;
			break;

		}
		
		
		//aqui hay que mejorarlo
		for (int y = 0; y < 70; y++)
		{
			for (int x = 0; x < 110; x++)
			{
				pantalla.mapa[x][y] = admin_ent.mapa.mapa[x][y];
			}
		}
		
		
		admin_ent.ejecutar_acciones();
		
		admin_ent.desplazar_camara(pantalla.screenWidth, pantalla.screenHeight);
	
		//esto definitivamente hay que arreglarlo
		pantalla.renderizar_mapa(admin_ent.camara.cam, admin_ent.mapa.clips, admin_ent.camara.cam.x, admin_ent.camara.cam.y);

		pantalla.renderizar_puntero(admin_ent.puntero.posicion_x, admin_ent.puntero.posicion_y, admin_ent.camara.cam, admin_ent.mapa.clips, admin_ent.mapa.usar_clip, admin_ent.objeto.usar_clip,  admin_ent.control.O);
		
	
		pantalla.mostrar_pantalla(admin_ent.v_actor_data, admin_ent.v_sobrePos, admin_ent.camara.cam, admin_ent.objeto.clips, admin_ent.vector_obj);

		frameTime = SDL_GetTicks() - frameStart;
		if (framedelay > frameTime)
		{
			//cout <<frameTime << endl;
			SDL_Delay(framedelay - frameTime);
		}
		
	}
	pantalla.cerrar();
	return 0;

}





int main(int argc, char *argv[])
{
	Juego juego;
	return juego.ejecutando();
}