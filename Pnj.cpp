#include "Pnj.h"



Pnj::Pnj()
{
}


Pnj::~Pnj()
{
}


void Pnj::cargar_entidad(int x, int y)
{
	for (int i = 0; i < 16; ++i)
	{
		clips[i].x = i * 120;
		clips[i].y = 0;
		clips[i].w = 120;
		clips[i].h = 120;
	}
	pnj.x = x;
	pnj.y = y;

}



void Pnj::desplazarDerecha()
{
	if (!colision_derecha)
	{
		
		derecha = true;
		contador += 0.10;
		pnj.x += 2;
		if ((usar_clip <= 12) && (contador >= 1))
		{
			usar_clip++;
			contador = 0.0;

		}
		if ((usar_clip <= 8) || (usar_clip >= 12))
		{
			usar_clip = 8;
		}
		//derecha = true;
	}
	else {
		//std::cout << "false" << endl;
		derecha = false;
	}
}

void Pnj::desplazarIzquierda()
{
	if (!colision_izquierda)
	{
		izquierda = true;
		contador += 0.10;
		pnj.x -= 2;


		if ((usar_clip <= 16) && (contador >= 1))
		{

			usar_clip++;
			contador = 0.0;

		}
		if ((usar_clip <= 12) || (usar_clip >= 16))
		{
			usar_clip = 12;
		}
	}
	else
	{
		izquierda = false;
	}
}

void Pnj::desplazarAbajo()
{

	if (!colision_abajo)
	{
		
		
		abajo = true;
		
	
		contador += 0.10;

		pnj.y += 2;



		if ((usar_clip <= 5) && (contador >= 1))
		{

			usar_clip++;
			contador = 0.0;

		}
		if ((usar_clip >= 4) || (usar_clip == 0))
		{
			usar_clip = 0;
		}
		
	}
	else
	{
		abajo = false;
	}
}

void Pnj::desplazarArriba()
{

	
	
	if (!colision_arriba)
	{
		
		arriba = true;
		contador += 0.10;

		pnj.y -= 2;

		if ((usar_clip <= 8) && (contador >= 1))
		{

			usar_clip++;
			contador = 0.0;

		}
		if ((usar_clip <= 4) || (usar_clip >= 8))
		{
			usar_clip = 4;
		}



	}
	else
	{
		//std::cout << "true" << endl;
		arriba = false;
	
	}
}

void Pnj::quieto()
{


	temp += 0.10;
	if ((abajo == false) && (arriba == false) && (derecha == false) && (izquierda == false))
	{
		if (usar_clip <= 3)
			usar_clip = 0;


		if ((usar_clip <= 7) && (usar_clip > 3))
			usar_clip = 4;

		if ((usar_clip <= 11) && (usar_clip > 7))
			usar_clip = 8;

		if ((usar_clip <= 16) && (usar_clip > 11))
			usar_clip = 12;

	}


}

void Pnj::esperar(int t)
{
	
}

void Pnj::ejecutar_estados()
{
	srand(time(NULL)); //inicializar num random
	random = 1 + rand() % 4; //guardar num random a partir de 1 hasta 4
	//std::cout << pnj.x << "  " << pnj.y << endl;
	
	for (int i = 0; i < accion.size(); i++)
	{
		//std::cout << accion[i] << endl;
		switch (accion[i])
		{

		case(0):
			if (temp >= 2)
			{ 
				accion.erase(accion.begin());
				temp = 0;
				accion.push_back(random); 
			}
			else {
				quieto();


				break;
			}


		case(1):
			if (colision_derecha) { accion.erase(accion.begin()); }
			else {

				desplazarDerecha();
				break;
			}

		case(2):
			if (colision_izquierda) { accion.erase(accion.begin()); }
			else {
				desplazarIzquierda();
				break;
			}

		case(3):
			//cout << "3" << endl;
			if (colision_abajo) { accion.erase(accion.begin()); }
			else {
				desplazarAbajo();
				break;
			}

		case(4):
			if (colision_arriba) { accion.erase(accion.begin()); }
			else {
				desplazarArriba();


				break;
			}

		default:
			break;
		}
		break;
	}

	//-----------------esta parte no se donde sea mejor ponerla

	//x1 = distancia desde izquierda hasta brazo izquierdo
	x1 = pnj.x + 36;

	//x2 = distancia desde izquierda hasta brazo derecho
	x2 = pnj.x + 82;

	// y1 = distancia hipotetica desde arriba hasta los talones
	y1 = pnj.y + 100;

	//y2 = distancia desde arriba de la imagen hasta la planta de los pies
	y2 = pnj.y + 110;

	//t = distancia desde arriba hasta la cabeza
	t = pnj.y + 8;

	//quieto();

	//-------------------


}

void Pnj::calcular_posicion(float rx, float ry)
{
	x_real = ((float)pnj.x) * rx;

	if ((x_real - floor(x_real)) < (ceil(x_real) - x_real))
	{
		floor(x_real);
	}
	else 	 ceil(x_real);



	y_real = ((float)pnj.y) * ry;
	
	if ((y_real - floor(y_real)) < (ceil(y_real) - y_real))
	{
		floor(y_real);
	}
	else 	 ceil(y_real);

	


}


void Pnj::colision(int objx, int objy, int tipo, int i, int tile_size)
{
	//array <int, 9> a0{ { pnj.x, pnj.y, clips[0].x, clips[0].y, clips[0].h, clips[0].w, usar_clip, col , 1} }; //es mas complicado de lo que pense. y pensaba que iba a ser muy complicado :�( 

	//colision_abajo= false;
	//colision con cajas
	
	//if(accion.size() >= 1)
	//std::cout << accion[0] << endl;

	int ox1{ 0 }, ox2{ 0 }, oy1{ 0 }, oy2{ 0 }, ot{ 0 }, ot2{ 0 };

	//colision con cajas
	if (tipo == 5)
	{
		ox1 = objx; ox2 = objx + tile_size; oy1 = objy; oy2 = objy + tile_size, ot = oy1 - (tile_size - 10), ot2 = objy;
	}

	//colision con muros vertical
	else if (tipo == 3)
	{
		ox1 = objx + 24; ox2 = objx + 34; oy1 = objy + 52; oy2 = objy + tile_size, ot = oy1 - 85, ot2 = objy;
	}

	//muros horizontal
	else if (tipo == 16 || 1)
	{
		ox1 = objx; ox2 = objx + tile_size; oy1 = objy + 52; oy2 = objy + tile_size, ot = oy1 - 85, ot2 = objy - 10;
	}

	//colision hacia abajo
	if ((x2 > ox1) && (x1 < ox2) && (y2 == oy1))
	{
		if (accion[0] == 3)
		{
			colision_abajo = true;
			accion.push_back(0);
		}
	}

	//colision con  hacia arriba
	if ((x2 > ox1) && (x1 < ox2) && (y1 == oy2))
	{
		if (accion[0] == 4)
		{
			colision_arriba = true;
			accion.push_back(0);
		}
	}

	//colision con el tile por delante
	if ((x2 > ox1) && (x1 < ox2) && (t > ot) && (t < oy2))
	{
		num_obj.push_back(i);
	}

	//colision con el tile por detras
	if ((x2 > ox1) && (x1 < ox2) && (y1 > ot2) && (y2 <= oy2))
	{
		num_obj2.push_back(i);
	}

	//colision hacia izquierda
	if ((x1 == ox2) && (y2 > oy1) && (y1 < oy2))
	{
		if (accion[0] == 2)
		{
			colision_izquierda = true;
			accion.push_back(0);
		}
	}

	//colision hacia derecha
	if ((x2 == ox1) && (y2 > oy1) && (y1 < oy2))
	{
		if (accion[0] == 1)
		{
			colision_derecha = true;
			accion.push_back(0);
		}
	}



}