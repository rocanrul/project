#include "AdminEntidad.h"



AdminEntidad::AdminEntidad()
{
}


AdminEntidad::~AdminEntidad()
{
}



void AdminEntidad::cargar_entidades()
{
	
	jugador1.cargar_entidad(100, 82);
	mapa.cargar_entidad(70, 110);
	objeto.cargar_entidad(60, 60); //hay que tratar de hacer que puntero y objeto trabajen juntos
	mapa.leer_mapa();
	objeto.leer_obj();
	jugador1.palto = 8000;
	jugador1.pancho = 10000;
	pnj.cargar_entidad(100, 500);
	jugador1.calcular_posicion(rx, ry);
	pnj.calcular_posicion(rx, ry);
												
													actor_data.rellenar(0,0, &jugador1.x_real, &jugador1.y_real, jugador1.clips, &jugador1.usar_clip, &jugador1.col_tile);
													v_actor_data.push_back(actor_data);
													actor_data.rellenar(1,1, &pnj.x_real, &pnj.y_real, pnj.clips, &pnj.usar_clip, &pnj.col);
													v_actor_data.push_back(actor_data);
													
													
													


	//los objetos se integran al vector de objetos
	for (int i = 0; i < objeto.vect_obj.size(); i++)
	{
		Objeto temp;
		temp.posicion_x = objeto.vect_obj[i].posicion_x;
		temp.posicion_y = objeto.vect_obj[i].posicion_y;
		temp.usar_clip = objeto.vect_obj[i].usar_clip;
		vector_obj.push_back(temp);
	}

}
//aqui se ejecuta la funcion de guardar
void AdminEntidad::guardar_entidades(bool n)
{
	if (n)
	{
		mapa.crear_mapa();
		objeto.crear_obj(vector_obj);
		cout << "El mapa se ha guardado correctamente" << endl;
	}
	
}

void AdminEntidad::desplazar_entidades()
{
	
	pnj.calcular_posicion(rx, ry);
	jugador1.calcular_posicion(rx, ry);
	objeto.crear_copia(camara.cam.x, camara.cam.y);
	//creo que seria mejor volver a juntarlos todos en uno solo xD
	if (!jugador1.colision_derecha)
	{
		jugador1.desplazarDerecha(control.derecha);
		
	}
	if (!jugador1.colision_izquierda)
	{
		jugador1.desplazarIzquierda(control.izquierda);

	}
	if (!jugador1.colision_abajo)
	{
		jugador1.desplazarAbajo(control.abajo);

	}
	if (!jugador1.colision_arriba)
	{
		jugador1.desplazarArriba(control.arriba);

	}

	//asignacion temporal de instrucciones o estados

	if (pnj.accion.size() < 1)
	{
		pnj.accion.push_back(4);
		pnj.accion.push_back(0);
		
	}
	
	pnj.ejecutar_estados();
	
	
	
	

	jugador1.quieto(control.derecha, control.izquierda, control.abajo, control.arriba, tile_size);
	

	puntero.desplazar(control.J, control.L, control.I, control.K, tile_size);

	if (!control.O)
	{
		mapa.desplazar(puntero.posicion_x, puntero.posicion_y, control.coma, control.punto, control.espacio);

	}
	else
	{
		objeto.desplazar(control.coma, control.punto);

		if (control.espacio)
		{
			if (!espacio)
			{
				espacio = true;
				Objeto obj_temporal;
				obj_temporal.posicion_x = puntero.posicion_x;
				obj_temporal.posicion_y = puntero.posicion_y;
				obj_temporal.usar_clip = objeto.usar_clip;
				vector_obj.push_back(obj_temporal);

				//metodo de ordenamiento por insercion
				
				int i, pos;


				for (i = 0; i < vector_obj.size(); i++)
				{
					pos = i;
					obj_temporal = vector_obj[i];

					while ((pos > 0) && (vector_obj[pos - 1].posicion_y > obj_temporal.posicion_y))
					{

						vector_obj[pos] = vector_obj[pos - 1];
						pos--;
					}
					vector_obj[pos] = obj_temporal;
				}
			}

		}

		if (!control.espacio)
		{
			espacio = false;
		}
	}
}


void AdminEntidad::desplazar_camara(int rx, int ry)
{
	camara.desplazar_cam(control.A, control.D, control.S, control.W, rx, ry, incremento);
	camara.seguir_jugador(jugador1.x_real, jugador1.y_real, control.derecha, control.izquierda, control.abajo, control.arriba, jugador1.colision_derecha, jugador1.colision_izquierda, jugador1.colision_arriba, jugador1.colision_abajo, rx, ry, incremento);

}

void AdminEntidad::detectar_colisiones()
{
	int obj_posx, obj_posy;
	pnj.num_obj.clear();
	pnj.num_obj2.clear();
	pnj.colision_abajo = false;
	pnj.colision_arriba = false;
	pnj.colision_derecha = false;
	pnj.colision_izquierda = false;
	pnj.col = 0;
	jugador1.num_obj.clear();
	jugador1.num_obj2.clear();
	jugador1.colision_abajo = false;
	jugador1.colision_arriba = false;
	jugador1.colision_derecha = false;
	jugador1.colision_izquierda = false;
	jugador1.col_tile = 0;
	v_sobrePos.clear();


	//comprobamos cada la entidad con todos los objetos
	for (int i = 0; i < vector_obj.size(); i++)
	{
		obj_posx = vector_obj[i].posicion_x * tile_size;//IMPORTANTE esto debo cambiarlo 
		obj_posy = vector_obj[i].posicion_y * tile_size;


		jugador1.colisiones(obj_posx, obj_posy, vector_obj[i].usar_clip, i, tile_size);
		pnj.colision(obj_posx, obj_posy, vector_obj[i].usar_clip, i, tile_size);

	}

	detectar_sobrepos(0, jugador1.jugador.y, jugador1.num_obj, jugador1.num_obj2);
	detectar_sobrepos(1, pnj.pnj.y, pnj.num_obj, pnj.num_obj2);
	
}



void AdminEntidad::detectar_sobrepos(int id, int y, std::vector <int> col_obj, std::vector <int> col_obj2)
{
	//posiblemente se pueda mejorar
	if (col_obj.size() != 0 || col_obj2.size() != 0)
	{
		SDL_Rect temp;
		//std::vector <int> v_temp1;
		//std::vector <int> v_temp2;


		if (col_obj.size() != 0)
		{
			
			if (col_obj2.size() != 0)
			{
				
				if (col_obj.size() >= col_obj2.size())
				{
					
					for (int i = 0; i < col_obj.size(); i++)
					{
						temp.h = col_obj[i];


						if (i > col_obj2.size() - 1)
						{
							temp.w = -1;
						}
						else
						{
							temp.w = col_obj2[i];
						}
						temp.x = id;
						temp.y = y;

						v_sobrePos.push_back(temp);
					}

				}
				else
				{
					
					for (int i = 0; i < col_obj2.size(); i++)
					{
						temp.w = col_obj2[i];


						if (i > col_obj.size() - 1)
						{
							temp.h = -1;
						}
						else
						{
							temp.h = col_obj[i];
						}
						temp.x = id;
						temp.y = y;

						v_sobrePos.push_back(temp);
					}

				}

			}
			else
			{
				
				for (int i = 0; i < col_obj.size(); i++)
				{
					
					temp.h = col_obj[i];
					temp.w = -1;
					temp.x = id;
					temp.y = y;

					v_sobrePos.push_back(temp);
				}
			}

		}
		else
		{
			
			for (int i = 0; i < col_obj2.size(); i++)
			{
				
				temp.w = col_obj2[i];
				temp.h = -1;
				temp.x = id;
				temp.y = y;

				v_sobrePos.push_back(temp);
			}
		}
		

		//ordenamiento en base a y
		for (int i = 0; i < v_sobrePos.size(); i++)
		{
			int pos = i;
			temp = v_sobrePos[i];

			while ((pos > 0) && (v_sobrePos[pos - 1].y > temp.y))
			{

				v_sobrePos[pos] = v_sobrePos[pos - 1];
				pos--;
			}
			v_sobrePos[pos] = temp;
		}

	}

}

void AdminEntidad::ejecutar_acciones()
{
	control.entradas();
	guardar_entidades(control.N);
	detectar_colisiones();
	desplazar_entidades();
	
}





void AdminEntidad::calcular_dimension(int rx_actual, int rx_ant, int ry_actual, int ry_ant, int tam_tile)
{

	///printf("%.6f", (float)rx_actual); //muestra las decimales

	rx = ((float)rx_actual) / ((float)rx_ant);
	ry = ((float)ry_actual) / ((float)ry_ant);
	//tile_size = tam_tile;
	float f_incremento = ((float)rx_actual * 2) / (float)rx_ant;
	
	//funcion de redondeo
	if ((f_incremento - floor(f_incremento)) < (ceil(f_incremento) - f_incremento))
	{
		floor(f_incremento);
	}
	else 	 ceil(f_incremento);

	incremento = (int)f_incremento;
	cout << incremento << endl;

}