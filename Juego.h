#pragma once
#include "Includes.h"

class Juego
{
public:
	Juego();
	int ejecutando();
	bool corriendo;
	virtual ~Juego();
	const float FPS = 60;
	const float framedelay = 1000 / FPS;

	float frameStart;
	float frameTime;
};

