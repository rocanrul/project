#include "Mapa.h"



Mapa::Mapa()
{
}


Mapa::~Mapa()
{
}

void Mapa::cargar_entidad(int x, int y)
{
	for (int i = 0; i < 10; ++i)
	{
		clips[i].x = i * 60;
		clips[i].y = 0;
		clips[i].w = 60;
		clips[i].h = 60;
	}
	ancho = x;
	alto = y;
}




void Mapa::desplazar(int pos_x, int pos_y, bool coma, bool punto, bool espacio)
{

	
	if (coma == true)
	{
		contador += 0.15;
		if (contador >= 1)
		{
			if (usar_clip == 0) {
				usar_clip = 11;
			}
			else {
				usar_clip -= 1;
			}
			contador = 0;
		}
	}

	if (punto == true)
	{

		contador += 0.15;
		if (contador >= 1)
		{
			if (usar_clip == 11) {
				usar_clip = 0;
			}
			else {
				usar_clip += 1;
			}
			contador = 0;
		}
		
	}
	if (espacio == true) {
		mapa[pos_x][pos_y] = usar_clip;
	}
}

void Mapa::leer_mapa()
{
	ifstream archivo;
	string texto, temporal;
	int x, y, clip;
	int desde, hasta;
	bool tam = false;

	archivo.open("mapa.txt", ios::in);

	if (archivo.fail())
	{
		cout << "No se pudo abrir el archivo" << endl;
		exit(1);
	}

	while (!archivo.eof())
	{
		getline(archivo, texto);
	}
	archivo.close();

	for (int i = 0; i < texto.size(); i++)
	{
		switch (texto[i])
		{
		case '<':
			desde = i + 1;
			y = -1;
			break;
		case ',':
			hasta = i;
			temporal = texto.substr(desde, hasta - 1);
			x = atoi(temporal.c_str());
			desde = hasta + 1;
			break;
		case 'c':
			hasta = i;
			temporal = texto.substr(desde, hasta - 1);
			y = atoi(temporal.c_str());
			desde = hasta + 1;
			break;
		case '>':
			hasta = i;
			temporal = texto.substr(desde, hasta + 1);
			if (y == -1)
			{
				y = atoi(temporal.c_str());
			}
			else
			{
				clip = atoi(temporal.c_str());
			}

			if (!tam)
			{

				tam = true;
				ancho = x;
				alto = y;
			}
			else {
				mapa[x][y] = clip;

			}
			break;

		default:
			break;
		}

		}

}



void Mapa::crear_mapa() 
{
	ofstream archivo;
	
		archivo.open("mapa.txt", ios::out);

		archivo << "<" << ancho << "," << alto << "> ";
		for (int y = 0; y < ancho; y++)
		{
			for (int x = 0; x < alto; x++)
			{
				archivo << "<" << x << "," << y << " c" << mapa[x][y]<<"> ";
			}
		}
		archivo.close();

}


