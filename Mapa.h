#pragma once
#include "Entidad.h"
class Mapa :
	public Entidad
{
public:
	Mapa();
	~Mapa();
	SDL_Rect clips[12];
	int usar_clip = 0;
	int alto, ancho;
	int mapa[110][70];
	SDL_Rect clips_mapa[12];
	void desplazar(int pos_x, int pos_y, bool coma, bool punto, bool espacio);
	void cargar_entidad(int x, int y);
	void leer_mapa();
	void crear_mapa();
};

