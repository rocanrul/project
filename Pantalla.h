#pragma once
#include "Includes.h"
#include "Objeto.h"
class Pantalla
{
	
public:
	Pantalla();
	bool iniciar_pantalla();
	SDL_Window *ventana;
	bool cargar_contenido();
	void limpiar_pantalla();
	int tam_tile_x, tam_tile_y; //esta variable es para ir preparando todo para que sea responsibo el juego
	int ancho_pantalla {1280}, alto_pantalla {720};
	Uint32 flags = 0;
	int screenWidth = ancho_pantalla, screenHeight = alto_pantalla;
	bool pantalla_completa(bool alt, bool enter);
	void mostrar_pantalla(std::vector <SDL_Rect> v_sobrePos, int x, int y, SDL_Rect camara, SDL_Rect clip[], int num_clip, SDL_Rect obj[], vector <Objeto> vector_obj, int colision, float pnj_x, float pnj_y, SDL_Rect pnj_clip[], int pnj_usarClip, int pnjcol, vector <int> pnjobj);
	void calcular_capas(vector <SDL_Rect> v_rect);
	void cerrar();
	int mapa[110][70];
	int mapa_ancho, mapa_alto;
	void renderizar_mapa(SDL_Rect cam, SDL_Rect clips_mapa[], int c_x, int c_y);
	void renderizar_objeto(SDL_Rect cam, SDL_Rect clips[], vector <Objeto> obj);
	void renderizar_jugador(float x, float y, SDL_Rect camara, SDL_Rect clip[], int num_clip);
	void renderizar_puntero(int px, int py, SDL_Rect cam, SDL_Rect clips[], int clip_mapa, int clip_obj, int cambiar);
	void renderizar_textura(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip);
	SDL_Texture *suelo;
	SDL_Texture *jugador;
	SDL_Texture *objetos;
	SDL_Texture *texto;
	SDL_Texture *pnj;
	TTF_Font *opensans;
	SDL_Color blanco {255,255,255};
	std::string ruta_imagen;
	SDL_Surface *mensaje;
	virtual ~Pantalla();

	//variable provisional
	bool ultimo;
	int c;
	vector <int> copia;
	int z = 0;
protected:
	SDL_Renderer * renderer;
	
};

