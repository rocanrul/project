#pragma once
#include "Entidad.h"
class Pnj :
	public Entidad
{
public:
	SDL_Rect clips[16];
	SDL_Rect pnj; //no me gusta el nombre de esta variable
	int usar_clip = 0;
	//int alto = 120, ancho = 120;
	void cargar_entidad(int x, int y); 
	void desplazarArriba();
	void desplazarAbajo();
	void desplazarDerecha();
	void desplazarIzquierda();
	void esperar(int t);
	bool derecha{ false }, izquierda{ false }, arriba{ false }, abajo{ false };
	void ejecutar_estados();
	void quieto();
	void colision(int objx, int objy,int tipo, int i, int tile_size);
	void calcular_posicion(float rx, float ry);
	std::vector <int> accion;
	std::vector <int> num_obj;
	std::vector <int> num_obj2;
	double temp = 0;
	int random;
	int col;
	Pnj();
	~Pnj();
};

