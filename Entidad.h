#pragma once
#include "Includes.h"

class Entidad
{
public:
	
	Entidad();
	//seria posible crear un sdl_rect general??
	int posicion_x, posicion_y; //hay que revisar detenidamente esta parte
	double contador = 0.0;
	virtual void cargar_entidad(int x, int y) = 0; //aun no me convence esta funcion
	int x1, x2, y1, y2, t;
	bool colision_abajo, colision_arriba, colision_derecha, colision_izquierda;
	float x_real, y_real;
	virtual ~Entidad();
};
