#pragma once
#include "Entidad.h"
class Objeto : //podria combinarse esta clase con clase puntero
	public Entidad
{
public:
	Objeto();
	~Objeto();
	SDL_Rect clips[17];
	int usar_clip = 0;
	SDL_Rect puntero = { 0,0,60,60 };
	int copia_obj[33][19];
	void crear_copia(int c_x, int c_y);
	vector <Objeto> vect_obj;
	void cargar_entidad(int x, int y);
	void desplazar(bool coma, bool punto);
	void crear_obj(vector <Objeto> vect_obj);
	void leer_obj();
};

