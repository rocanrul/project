#include "Control.h"



Control::Control()
{
}


Control::~Control()
{
}

void Control::entradas()
{
	while (SDL_PollEvent(&evento))
	{
		if (salir(&evento) == true)
		{
			
		}
		eventos(&evento);

	}
}


void Control::eventos(SDL_Event *evento)
{

	if (evento->type == SDL_KEYDOWN)
	{
		switch (evento->key.keysym.sym)
		{
		case SDLK_RIGHT:
			derecha = 1;
			break;

		case SDLK_LEFT:
			izquierda = 1;
			break;

		case SDLK_DOWN:
			abajo = 1;
			break;

		case SDLK_UP:
			arriba = 1;
			break;

		case SDLK_a:
			A = 1;
			break;

		case SDLK_d:
			D = 1;
			break;

		case SDLK_s:
			S = 1;
			break;

		case SDLK_w:
			W = 1;
			break;

		case SDLK_j:
			J = 1;
			break;

		case SDLK_l:
			L = 1;
			break;

		case SDLK_i:
			I = 1;
			break;

		case SDLK_k:
			K = 1;
			break;

		case SDLK_n:
			N = 1;
			break;
			
		case SDLK_COMMA:
			coma = 1;
			break;

		case SDLK_PERIOD:
			punto = 1;
			break;

		case SDLK_SPACE:
			espacio = 1;
			break;
			
		case SDLK_LALT:
			alt_l = 1;
			break;

		case SDLK_RETURN:
			enter = 1;	
			break;


		default:
			break;
		}
	}

	if (evento->type == SDL_KEYUP)
	{
		switch (evento->key.keysym.sym)
		{
		case SDLK_RIGHT:
			derecha = 0;
			break;

		case SDLK_LEFT:
			izquierda = 0;
			break;

		case SDLK_DOWN:
			abajo = 0;
			break;

		case SDLK_UP:
			arriba = 0;
			break;

		case SDLK_a:
			A = 0;
			break;

		case SDLK_d:
			D = 0;
			break;

		case SDLK_s:
			S = 0;
			break;

		case SDLK_w:
			W = 0;
			break;

		case SDLK_j:
			J = 0;
			break;

		case SDLK_l:
			L = 0;
			break;

		case SDLK_i:
			I = 0;
			break;

		case SDLK_k:
			K = 0;
			break;

		case SDLK_n:
			N = 0;
			break;

		case SDLK_o:
			if (O)
			{
				O = 0;
			}
			else
			{
				O = 1;
			}
			
			break;

		case SDLK_COMMA:
			coma = 0;
			break;

		case SDLK_PERIOD:
			punto = 0;
			break;

		case SDLK_SPACE:
			espacio = 0;
			break;

		case SDLK_LALT:
			alt_l = 0;
			break;

		case SDLK_RETURN:
			enter = 0;
			break;

		default:
			break;
		}

	}

}

bool Control::salir(SDL_Event *evento)
{
	if (evento->type == SDL_QUIT)
	{
		cerrar = true;
		return true;
	}
	
}