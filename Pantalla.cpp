#include "Pantalla.h"



Pantalla::Pantalla()
{
}


Pantalla::~Pantalla()
{
}


bool Pantalla::iniciar_pantalla()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		return false;
	}

	TTF_Init();
	//-----------------nombre de la ventana, pos x, pos y, ancho, alto ,flags/
	if ((ventana = SDL_CreateWindow("Juego", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, ancho_pantalla, alto_pantalla, 0 | SDL_WINDOW_OPENGL)) == NULL)
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return false;
	}
	if ((renderer = SDL_CreateRenderer(ventana, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)) == NULL)
	{
		SDL_DestroyWindow(ventana);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}
	tam_tile_x = 60;// es necesario esto??
	tam_tile_y = 60;

	//tal vez luego sea mejor usar un nintendo switch
	//si queremos meter otras resoluciones usando las mismas proporciones tendremos que hacer varios ajustes
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
	SDL_RenderSetLogicalSize(renderer, 1920, 1080);


	return true;

}


bool Pantalla::cargar_contenido()
{
	ruta_imagen = "ground.PNG";
	suelo = IMG_LoadTexture(renderer, ruta_imagen.c_str());
	ruta_imagen = "player.PNG";
	jugador = IMG_LoadTexture(renderer, ruta_imagen.c_str());
	ruta_imagen = "objects.PNG";
	objetos = IMG_LoadTexture(renderer, ruta_imagen.c_str());
	ruta_imagen = "pnj.PNG";
	pnj = IMG_LoadTexture(renderer, ruta_imagen.c_str());
	opensans = TTF_OpenFont("OpenSans.ttf", 20);
	mensaje = TTF_RenderText_Solid(opensans, "version 0.4", blanco);
	texto = SDL_CreateTextureFromSurface(renderer, mensaje);

	return true;
}


void Pantalla::limpiar_pantalla()
{
	SDL_RenderClear(renderer);
}

void Pantalla::renderizar_textura(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip)
{

	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	

	//esto se puede mejorar facilmente
	if (tam_tile_x == 60)
	{
		

		if (clip != NULL)
		{
			
			dst.w = clip->w;
			dst.h = clip->h;
		}
		else {
			SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
		}

		SDL_RenderCopy(ren, tex, clip, &dst);
		
	}

	else if (tam_tile_x !=60)
	{
		
		//t.x = x;
		//t.y = y;

		if (clip != NULL)
		{
			dst.w =   ((1920 * 10) / (ancho_pantalla) * clip->w)/10; //esta operacion esta bastante pendeja
			dst.h =   ((1080 * 10) / (alto_pantalla) * clip->h)/10;
			
		}
		else {
			SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
		}

		SDL_RenderCopy(ren, tex, clip, &dst);
	}
}

void Pantalla::calcular_capas(vector <SDL_Rect> v_rect)
{
	int aux;
	for (int i = 0; i < v_rect.size(); i++)
	{
		aux = i;

		if (v_rect[i].w != -1)
		{
			cout << "capa 0";
		}



	}

}


void Pantalla::mostrar_pantalla(std::vector <Actor_data> v_actor_data, std::vector <SDL_Rect> v_sobrePos, SDL_Rect camara, SDL_Rect obj[], vector <Objeto> vector_obj)
{

	//SDL_SetTextureColorMod(jugador, 60, 80, 80);
	
	//todos los objetos por defecto se dibujan primero que los personajes

	SDL_SetTextureColorMod(suelo, 255, 255, 255);
	SDL_SetTextureColorMod(jugador, 255, 255, 255);
	SDL_SetTextureColorMod(objetos, 255, 255, 255);
	

	
	//ordenamiento de sobreposiciones o capas	
	if (v_sobrePos.size() != 0) //si existe una sopreposicion
	{
		for (int j = 0; j < vector_obj.size(); j++) //recorre todos los objetos
		{
			for (unsigned int i = 0; i < v_sobrePos.size(); i++) //recorre unicamente los objetos que estan en conflicto de sobreposicionamiento
			{
				//cout << v_rect[i].x << endl;
				if (v_sobrePos[i].h != -1) //dibuja lo que este detras de los personajes (-1 significa nulo mientras que cero o un valor positivo indica el id del actor en conflicto)
				{

					renderizar_textura(objetos, renderer, (-camara.x + (vector_obj[v_sobrePos[i].h].posicion_x * tam_tile_x)), (-camara.y + (vector_obj[v_sobrePos[i].h].posicion_y * tam_tile_y)), &obj[vector_obj[v_sobrePos[i].h].usar_clip]);
				}

				switch (v_sobrePos[i].x) //el identificador x se�ala el id del actor con el cual esta en conflicto
				{
					//en base al identificador se imprime en orden cada entidad
				case 0:

					renderizar_actores(v_actor_data, camara,v_sobrePos[i].x);
					break;

				case 1:
				
					renderizar_actores(v_actor_data, camara, v_sobrePos[i].x);
					break;


				default:
					break;

				}

				for (unsigned int k = 0; k < v_sobrePos.size(); k++) //se ejecuta una segunda vuelta para asegurar que los objetos que estan delante se dibujen primero. (TAL VEZ SE PUEDA MEJORAR...)
				{

					if (v_sobrePos[k].w != -1) //dibuja lo que este delante de los personajes (-1 significa nulo mientras que cero o un valor positivo indica el id del actor en conflicto)
					{
						//cout << v_rect[k].w << endl;
						renderizar_textura(objetos, renderer, (-camara.x + (vector_obj[v_sobrePos[k].w].posicion_x * tam_tile_x)), (-camara.y + (vector_obj[v_sobrePos[k].w].posicion_y * tam_tile_y)), &obj[vector_obj[v_sobrePos[k].w].usar_clip]);
					}


					if (j == v_sobrePos[k].w || j == v_sobrePos[i].h)
					{
						if (v_sobrePos[k].w != vector_obj.size() - 1)
						{
							j++;
						}
						else
						{
							break;
						}

						//aun hay bugs 
						if (v_sobrePos[i].h != vector_obj.size() - 1) //no se por que pero esto tiene que estar exactamente asi, si no se emputa :(
						{
							//j++;
						}
						else
						{
							break;
						}

					}
					
					renderizar_textura(objetos, renderer, (-camara.x + (vector_obj[j].posicion_x * tam_tile_x)), (-camara.y + (vector_obj[j].posicion_y * tam_tile_y)), &obj[vector_obj[j].usar_clip]);

					
				}
			}
		}	
		
	}
	else
	{

		for (int j = 0; j < vector_obj.size(); j++)
		{
			renderizar_textura(objetos, renderer, (-camara.x + (vector_obj[j].posicion_x * tam_tile_x)), (-camara.y + (vector_obj[j].posicion_y * tam_tile_y)), &obj[vector_obj[j].usar_clip]);
		}
		
			renderizar_actores(v_actor_data, camara, 2);		
	}


	renderizar_textura(texto, renderer, 0, 0, NULL);
	

	SDL_RenderPresent(renderer);
}



/*void Pantalla::renderizar_jugador(float x, float y, SDL_Rect camara, SDL_Rect clip[], int num_clip)
{
	
		renderizar_textura(jugador, renderer, (int) x - camara.x,(int) y -camara.y, &clip[num_clip]);
	
}*/

void Pantalla::renderizar_actores(std::vector <Actor_data> v_actordata, SDL_Rect camara, int id_col)
{
	Actor_data temp;


	for (int i = 0; i < v_actordata.size(); i++)
	{
		if (v_actordata[i].id == id_col)
		{
			temp = v_actordata[i];
			v_actordata2.push_back(temp);
			v_actordata.erase(v_actordata.begin() + i);
		}
	}

	for (int i = 0; i < v_actordata.size(); i++) //ordenamiento
	{
		int pos = i;
		temp = v_actordata[i];

		while ((pos > 0) && (*v_actordata[pos - 1].p_y > *temp.p_y))
		{

			v_actordata[pos] = v_actordata[pos - 1];
			pos--;
		}
		v_actordata[pos] = temp;
	}


	for (int i = 0; i < v_actordata.size(); i++)
	{

		if (v_actordata[i].tipo == 0)
		{
			renderizar_textura(jugador, renderer, (int)*v_actordata[i].p_x - camara.x, (int)*v_actordata[i].p_y - camara.y, &v_actordata[i].p_clips[(int)*v_actordata[i].p_clip]);
		}
			

		if (v_actordata[i].tipo == 1)
		{
			renderizar_textura(pnj, renderer, (int)*v_actordata[i].p_x - camara.x, (int)*v_actordata[i].p_y - camara.y, &v_actordata[i].p_clips[(int)*v_actordata[i].p_clip]);
		}
			

	}
	
}



void Pantalla::cerrar()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(ventana);
	TTF_Quit();
	SDL_Quit();	
}



void Pantalla::renderizar_mapa(SDL_Rect cam, SDL_Rect clips_mapa[], int c_x, int c_y)
{

	int y1 = ((c_y / tam_tile_x) + (((alto_pantalla / tam_tile_x) / 2) + 3 )) - (((alto_pantalla / tam_tile_x) / 2) + 3); //de donde sale ese mas tres??
	int x1 = ((c_x / tam_tile_x) + (((ancho_pantalla / tam_tile_x) / 2) + 3 )) - (((ancho_pantalla / tam_tile_x) / 2) + 3);
	int y2 = ((c_y / tam_tile_x) + (((alto_pantalla / tam_tile_x) / 2) + 3 )) + (((alto_pantalla / tam_tile_x) / 2) + 3);
	int x2 = ((c_x / tam_tile_x) + (((ancho_pantalla / tam_tile_x) / 2) + 3)) + (((ancho_pantalla / tam_tile_x) / 2) + 3);

	if (x1 <= 0)
		x1 = 0;
	if (y1 <= 0)
		y1 = 0;
	if (x2 >= 110)
		x2 = 110;
	if (y2 >= 70)
		y2 = 70;
	for (int y = y1 ; y < y2; y++)
	{
		for (int x = x1; x < x2; x++)
		{
			
			renderizar_textura(suelo, renderer, (-cam.x + (x * tam_tile_x)), (-cam.y + (y * tam_tile_y)), &clips_mapa[mapa[x][y]]);
			
		}

	}
	
}



void Pantalla::renderizar_objeto(SDL_Rect cam, SDL_Rect clips[], vector <Objeto> obj)
{
	for (int i = 0; i < obj.size(); i++)
	{

		renderizar_textura(objetos, renderer, (-cam.x + (obj[i].posicion_x * tam_tile_x)), (-cam.y + (obj[i].posicion_y *tam_tile_y)), &clips[obj[i].usar_clip]);
		
	}
}



void Pantalla::renderizar_puntero(int px, int py, SDL_Rect cam, SDL_Rect clips[], int clip_mapa,int clip_obj, int cambiar)
{

	if (!cambiar)
	{
		SDL_SetTextureColorMod(suelo, 200, 200, 200);
		renderizar_textura(suelo, renderer, (px * tam_tile_x) - cam.x, (py * tam_tile_y) - cam.y, &clips[clip_mapa]);
	}
	else
	{
		SDL_SetTextureColorMod(objetos, 200, 240, 200);
		renderizar_textura(objetos, renderer, (px * tam_tile_x) - cam.x, (py * tam_tile_y) - cam.y, &clips[clip_obj]);
	}

	
}



bool Pantalla::pantalla_completa(bool alt, bool enter)
{
	if (alt && enter)
	{
		
		

		SDL_SetRelativeMouseMode((SDL_bool)!SDL_GetRelativeMouseMode());
		if (flags == 0)
		{
			flags = 1;
			int i = SDL_GetWindowDisplayIndex(ventana);
			screenWidth = ancho_pantalla;
			screenHeight = alto_pantalla;
			SDL_SetWindowFullscreen(ventana, 0);
			SDL_SetWindowBordered(ventana, SDL_TRUE);
			SDL_SetWindowSize(ventana, screenWidth, screenHeight);

			//TODO: This doesn't center the window on the appropriate display
			SDL_SetWindowPosition(ventana, SDL_WINDOWPOS_CENTERED_DISPLAY(i), SDL_WINDOWPOS_CENTERED_DISPLAY(i));
			tam_tile_x = 60;
			tam_tile_y = 60;
			//cout << " x" << tam_tile_x << "   y" << tam_tile_y << endl;
			
		}
		else if (flags == 1)
		{
			//SDL_RestoreWindow(ventana); //Incase it's maximized...
			flags = 0;
			int i = SDL_GetWindowDisplayIndex(ventana);
			SDL_Rect j;
			SDL_GetDisplayBounds(i, &j);
			screenWidth = j.w;
			screenHeight = j.h;
		//	printf("w %d, h%d, x %d, y %d", screenHeight, screenWidth, j.x, j.y, "\n  ");
			SDL_SetWindowFullscreen(ventana, SDL_WINDOW_FULLSCREEN_DESKTOP);
			SDL_SetWindowBordered(ventana, SDL_FALSE);

			//This isn't needed?
			SDL_SetWindowPosition(ventana, SDL_WINDOWPOS_CENTERED_DISPLAY(i), SDL_WINDOWPOS_CENTERED_DISPLAY(i));
			SDL_SetRelativeMouseMode(SDL_TRUE);

			tam_tile_x = tam_tile_x * ((float)screenWidth) / ((float)ancho_pantalla);
			tam_tile_y = tam_tile_y * ((float)screenHeight) / ((float)alto_pantalla);
			//cout << " x" << tam_tile_x << "   y" << tam_tile_y << endl;
			//tile_size = tam_tile;
			

			
		}

		//recalculateResolution(); //This function sets appropriate font sizes/UI positions
		return true;
	}
	
	return false;
}
	


