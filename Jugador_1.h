#pragma once
#include "Entidad.h"

class Jugador_1 : public Entidad
{
public:
	Jugador_1();
	SDL_Rect clips[16];
	SDL_Rect jugador;
	int usar_clip = 0;
	//int alto = 120, ancho = 120;
	//SDL_Rect desplazar(bool derecha, bool izquierda, bool abajo, bool arriba, SDL_Rect camara);
	void quieto(bool derecha, bool izquierda, bool abajo, bool arriba, int tile_size);
	void cargar_entidad(int x, int y);
	void desplazarArriba(bool arriba);
	void desplazarAbajo(bool abajo);
	void desplazarDerecha(bool derecha);
	void desplazarIzquierda(bool izquierda);
	int palto, pancho;
	void colisiones(int objx, int objy, int tipo, int i, int tile_size);
	void calcular_posicion(float rx, float ry);
	int col_tile;
	std::vector <int> num_obj;
	std::vector <int> num_obj2;
	~Jugador_1();
};

